# GitLab CI Archiver

(not associated with GitLab)

This is a program for archiving GitLab CI builds to a Git repository.

Build and run with:
```bash
cargo build --workspace --release
./gitlab-ci-archiver --help
./email-manage --help
```

# Send someone an email with a link allowing them to edit their subscriptions (after Initial Setup):

Change the email address in the following command:
```bash
sudo -H -u build /bin/bash -c 'cd /home/build/gitlab-ci-archiver; ./email-manage send-unsubscribe-auth-email someone@example.com'
```

# Initial Setup

Create `build` user:

```bash
sudo adduser --system --group --disabled-login build
```

Clone git repo into `build`'s home:
```bash
sudo -H -u build /bin/bash
cd ~
git clone https://salsa.debian.org/Kazan-team/gitlab-ci-archiver.git
cd gitlab-ci-archiver
```

Install Rust via Rustup:
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Build:
```bash
cargo build --workspace --release
```

Create a git repo where you want the CI builds to be archived:
```bash
git init ./build-archive.git
```

Create `config.toml` file:

```bash
cp example-config.toml config.toml
nano config.toml
```

Temporarily Change `email`'s `command` field to `/bin/true`:
```toml
[email]
command = ["/bin/true"]
```

Change the `projects` keys to match your installation.

Change `gitlab`'s `server_host_name` to match your installation.
This is the GitLab server to pull build logs from.

Fill in the `access_token` field with a GitLab personal access token.
The token needs to have the `api` permission (though the `read_api` permission might work instead).

See [GitLab's docs](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) for instructions.

Run `gitlab-ci-archiver` in `--full` mode to copy all build logs to the git repo (otherwise it only checks for recent builds).

```bash
./gitlab-ci-archiver poll --config config.toml --full
```

Edit `email-config.toml`:
change urls to match your system.

Create the key for generating unsubscribe urls:
```bash
./email-manage generate-key
```

Edit `config.toml` again:
Change `email`'s `command` field to the correct value:
```toml
[email]
command = ["./send-email.sh", "send"]
```

Edit `send-email-config` and change the values to suit your installation.

Add the `unsubscribe` URL to your nginx installation:
Change `fcgiwrap` to run as the same user as gitlab-ci-archiver (the `build` user), so `unsubscribe` can read the key for generating unsubscribe urls, as well as edit email subscription state:
In a separate shell (not in `build` user):
```bash
sudo systemctl edit --full fcgiwrap.service
```
Change both `User` and `Group` to the same user/group as gitlab-ci-archiver (the `build` user).
Restart `fcgiwrap`:
```bash
sudo systemctl restart fcgiwrap.service
```
Edit `/etc/nginx/` and add the `unsubscribe` URL:
```
location /unsubscribe {
    fastcgi_pass unix:/var/run/fcgiwrap.socket;
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME /home/build/gitlab-ci-archiver/unsubscribe;
}
```
Restart `nginx`:
```bash
sudo systemctl restart nginx.service
```

Install `gitlab-ci-archiver.service` and `gitlab-ci-archiver.timer` using systemd:
```bash
systemctl enable /home/build/gitlab-ci-archiver/gitlab-ci-archiver.service /home/build/gitlab-ci-archiver/gitlab-ci-archiver.timer
systemctl start gitlab-ci-archiver.timer
```

Now your local repo has all the build logs in it. If you want to push it to a remote, you will need to run the required `git` commands since `gitlab-ci-archiver` doesn't currently push automatically.

# Shell autocompletion

## bash
Add the following command to your login script:

```bash
exec "$(path/to/target/debug/gitlab-ci-archiver completions bash)"
```

## Other shells

Use an analogous command to the bash autocompletion command above.



