use lettre::EmailAddress;
use serde::Deserialize;
use std::{
    collections::BTreeSet,
    fmt, fs,
    num::NonZeroU32,
    path::{Path, PathBuf},
};
use url::Url;

#[derive(Deserialize, Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
#[serde(try_from = "Url")]
pub struct UrlWithoutQueryString(Url);

impl fmt::Display for UrlWithoutQueryString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl std::ops::Deref for UrlWithoutQueryString {
    type Target = Url;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Debug)]
pub struct UrlHasQueryString;

impl fmt::Display for UrlHasQueryString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("URL has a query string")
    }
}

impl std::error::Error for UrlHasQueryString {}

impl TryFrom<Url> for UrlWithoutQueryString {
    type Error = UrlHasQueryString;

    fn try_from(value: Url) -> Result<Self, Self::Error> {
        if value.query().is_some() {
            Err(UrlHasQueryString)
        } else {
            Ok(UrlWithoutQueryString(value))
        }
    }
}

impl From<UrlWithoutQueryString> for Url {
    fn from(value: UrlWithoutQueryString) -> Self {
        value.0
    }
}

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct UnsubscribeEmailRateLimit {
    #[serde(with = "humantime_serde")]
    pub period: std::time::Duration,
    pub no_delay_count: NonZeroU32,
    #[serde(with = "humantime_serde")]
    pub max_delay: std::time::Duration,
    #[serde(with = "humantime_serde")]
    pub reset_duration: std::time::Duration,
}

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct Config {
    pub unsubscribe_url_key_file: PathBuf,
    pub unsubscribe_url: UrlWithoutQueryString,
    pub emails_state_dir: PathBuf,
    #[serde(rename = "gitlab_ci_archiver_config")]
    pub gitlab_ci_archiver_config_path: PathBuf,
    #[serde(with = "humantime_serde")]
    pub unsubscribe_url_valid_duration: std::time::Duration,
    pub unsubscribe_email_rate_limit: UnsubscribeEmailRateLimit,
    pub sendmail_command: PathBuf,
    pub unsubscribe_email_from: Option<EmailAddress>,
    pub unsubscribe_email_reply_to: Option<EmailAddress>,
    #[serde(skip)]
    gitlab_ci_archiver_config_contents: Option<gitlab_ci_archiver_config::Config>,
    #[serde(skip)]
    projects: BTreeSet<String>,
}

impl Config {
    pub fn gitlab_ci_archiver_config(&self) -> &gitlab_ci_archiver_config::Config {
        self.gitlab_ci_archiver_config_contents
            .as_ref()
            .expect("should have been loaded by read_config")
    }
    pub fn projects(&self) -> &BTreeSet<String> {
        &self.projects
    }
}

pub fn read_config(config_file_name: impl AsRef<Path>) -> Result<Config, clap::Error> {
    fn map_err(config_file_name: &Path, err: impl fmt::Display) -> clap::Error {
        clap::Error::raw(
            clap::ErrorKind::Io,
            format_args!(
                "Error reading config file \"{}\": {}\n",
                config_file_name.display(),
                err
            ),
        )
    }
    let config_file_name = config_file_name.as_ref();
    let file_contents = fs::read(config_file_name).map_err(|err| map_err(config_file_name, err))?;
    let mut config: Config =
        toml::from_slice(&file_contents).map_err(|err| map_err(config_file_name, err))?;
    config.gitlab_ci_archiver_config_contents = Some(gitlab_ci_archiver_config::read_config(
        &config.gitlab_ci_archiver_config_path,
    )?);
    let mut projects: Vec<_> = config
        .gitlab_ci_archiver_config()
        .projects
        .iter()
        .map(|v| v.path.clone())
        .collect();
    // BTreeSet sorts in FromIterator anyway, so let's check for duplicates too
    projects.sort_unstable();
    if let Some(index) = projects.windows(2).position(|v| v[0] == v[1]) {
        return Err(map_err(
            &config.gitlab_ci_archiver_config_path,
            format_args!("duplicate project: {:?}", projects[index]),
        ));
    }
    config.projects = projects.into_iter().collect();
    Ok(config)
}

pub fn read_config_or_exit(config_file_name: impl AsRef<Path>) -> Config {
    match read_config(config_file_name) {
        Ok(v) => v,
        Err(err) => err.exit(),
    }
}
