use crate::config::Config;
use anyhow::{ensure, Context, Result};
use chrono::{DateTime, Utc};
use lettre::EmailAddress;
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use std::{
    borrow::Cow,
    collections::BTreeSet,
    fmt,
    fs::{self, File, OpenOptions},
    io::{BufReader, ErrorKind, Write},
    path::PathBuf,
    time::Duration,
};
use tai64::Tai64N;

#[derive(Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct EmailState {
    pub last_sent_unsubscribe_time: Option<Tai64N>,
    pub rate_limit_step: u32,
    pub allowed_projects: BTreeSet<String>,
    pub email: EmailAddress,
}

impl EmailState {
    pub fn new(email: EmailAddress) -> Self {
        Self {
            last_sent_unsubscribe_time: None,
            rate_limit_step: 0,
            allowed_projects: BTreeSet::new(),
            email,
        }
    }
}

#[derive(Debug, Clone)]
pub struct UnsubscribeEmailRateLimited {
    pub must_wait_till: Tai64N,
}

impl fmt::Display for UnsubscribeEmailRateLimited {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "sent too many unsubscribe emails -- try again after {}",
            DateTime::<Utc>::from(self.must_wait_till.to_system_time()).to_rfc2822()
        )
    }
}

impl std::error::Error for UnsubscribeEmailRateLimited {}

impl Config {
    pub fn email_state_path(&self, email: &EmailAddress) -> Result<PathBuf> {
        let email: &str = email.as_ref();
        let hex = format!("{:x}", Sha256::digest(email));
        let mut retval = self.emails_state_dir.join(&hex[..2]);
        retval.push(&hex[2..]);
        retval.set_extension("json");
        Ok(retval)
    }
    pub fn is_email_allowed(&self, email: &EmailAddress, project: &str) -> Result<bool> {
        Ok(self
            .get_email_state(email)?
            .allowed_projects
            .contains(project))
    }
    pub fn set_email_allowed(
        &self,
        email: &EmailAddress,
        project: Cow<str>,
        allowed: bool,
    ) -> Result<()> {
        let mut state = self.get_email_state(email)?;
        if allowed {
            state.allowed_projects.insert(project.into_owned());
        } else {
            state.allowed_projects.remove(&*project);
        }
        self.set_email_state(&state)
    }
    pub fn get_email_state(&self, email: &EmailAddress) -> Result<EmailState> {
        let path = self.email_state_path(email)?;
        Ok(match File::open(&path) {
            Err(e) if e.kind() == ErrorKind::NotFound => EmailState::new(email.clone()),
            file => {
                let file = file.with_context(|| {
                    format!("failed to read email state json {}", path.display())
                })?;
                let retval: EmailState = serde_json::from_reader(BufReader::new(file))
                    .with_context(|| {
                        format!("failed to read email state json {}", path.display())
                    })?;
                ensure!(retval.email == *email, "file has wrong email address");
                retval
            }
        })
    }
    pub fn set_email_state(&self, state: &EmailState) -> Result<()> {
        let path = self.email_state_path(&state.email)?;
        fs::create_dir_all(path.parent().expect("should have parent path")).with_context(|| {
            format!(
                "failed to create parent directories of email state json {}",
                path.display()
            )
        })?;
        let temp_path = path.with_extension("tmp");
        let mut file = OpenOptions::new()
            .write(true)
            .create_new(true)
            .open(&temp_path)
            .with_context(|| {
                format!(
                    "failed to create email state json temporary file {}",
                    temp_path.display()
                )
            })?;
        file.write_all(&serde_json::to_vec(state)?)
            .with_context(|| format!("failed to write email state json {}", path.display()))?;
        file.sync_all()
            .with_context(|| format!("failed to write email state json {}", path.display()))?;
        drop(file);
        // do write and then rename, rather than just writing directly,
        // because it atomically replaces `path`
        fs::rename(temp_path, &path).with_context(|| {
            format!("failed to rename email state json file {}", path.display())
        })?;
        Ok(())
    }
    pub fn reset_unsubscribe_email_rate_limit(&self, email: &EmailAddress) -> Result<()> {
        let mut state = self.get_email_state(email)?;
        state.last_sent_unsubscribe_time = None;
        state.rate_limit_step = 0;
        self.set_email_state(&state)
    }
    /// gets the last send unsubscribe time, but returns `None` if it's old enough to be reset
    pub fn get_checked_last_sent_unsubscribe_time(&self, state: &EmailState) -> Option<Tai64N> {
        self.get_checked_last_sent_unsubscribe_time_with_now(state, Tai64N::now())
    }
    /// gets the last send unsubscribe time, but returns `None` if it's old enough to be reset
    pub fn get_checked_last_sent_unsubscribe_time_with_now(
        &self,
        state: &EmailState,
        now: Tai64N,
    ) -> Option<Tai64N> {
        let last_time = state.last_sent_unsubscribe_time?;
        let dur = now.duration_since(&last_time).unwrap_or(Duration::ZERO);
        if dur < self.unsubscribe_email_rate_limit.reset_duration {
            Some(last_time)
        } else {
            None
        }
    }
    pub fn check_send_unsubscribe_email(
        &self,
        email: &EmailAddress,
    ) -> Result<Result<(), UnsubscribeEmailRateLimited>> {
        let limit_config = &self.unsubscribe_email_rate_limit;
        let mut state = self.get_email_state(email)?;
        let now = Tai64N::now();
        if let Some(last_time) = self.get_checked_last_sent_unsubscribe_time_with_now(&state, now) {
            let dur = now.duration_since(&last_time).unwrap_or(Duration::ZERO);
            state.rate_limit_step = state.rate_limit_step.saturating_add(1);
            let delay = if let Some(delay_step) = state
                .rate_limit_step
                .checked_sub(limit_config.no_delay_count.get())
            {
                limit_config
                    .period
                    .saturating_mul(2u32.saturating_pow(delay_step))
                    .min(limit_config.max_delay)
            } else {
                Duration::ZERO
            };
            if dur < delay {
                return Ok(Err(UnsubscribeEmailRateLimited {
                    must_wait_till: last_time + delay,
                }));
            }
        } else {
            state.rate_limit_step = 0;
        }
        state.last_sent_unsubscribe_time = Some(now);
        Ok(Ok(self.set_email_state(&state)?))
    }
}
