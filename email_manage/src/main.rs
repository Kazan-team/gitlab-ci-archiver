use crate::{
    config::{read_config_or_exit, Config},
    unsubscribe_url::Key,
};
use anyhow::{ensure, Result};
use clap::{CommandFactory, Parser};
use clap_complete::{generate, Shell};
use lettre::EmailAddress;
use std::{io::stdout, process::exit};
use url::Url;

pub mod config;
pub mod email_state;
pub mod send_email;
pub mod unsubscribe_cgi;
pub mod unsubscribe_url;

#[derive(Parser, Debug)]
#[clap(
    name = "email-manage",
    about = "Manages email addresses for sending build failure notifications"
)]
pub struct EmailManageCLI {
    #[clap(subcommand)]
    pub command: Command,
}

#[derive(Parser, Debug)]
pub struct CompletionsCommand {
    shell: Shell,
}

impl CompletionsCommand {
    pub fn run(self) -> Result<()> {
        generate(
            self.shell,
            &mut EmailManageCLI::command(),
            "email-manage",
            &mut stdout(),
        );
        generate(
            self.shell,
            &mut UnsubscribeCLI::command(),
            "unsubscribe",
            &mut stdout(),
        );
        Ok(())
    }
}

#[derive(Parser, Debug)]
pub struct ConfigOpt {
    /// The config file path
    #[clap(short, long, parse(from_os_str = read_config_or_exit), default_value = "email-config.toml")]
    pub config: Config,
}

impl std::ops::Deref for ConfigOpt {
    type Target = Config;

    fn deref(&self) -> &Self::Target {
        &self.config
    }
}

#[derive(Parser, Debug)]
pub struct GenerateKeyCommand {
    #[clap(flatten)]
    pub config: ConfigOpt,
}

impl GenerateKeyCommand {
    pub fn run(self) -> Result<()> {
        Key::generate()?.write_openssh_file(&self.config.unsubscribe_url_key_file)?;
        let key_file = self.config.unsubscribe_url_key_file.display();
        println!("Generated key {key_file}");
        Ok(())
    }
}

#[derive(Parser, Debug)]
pub struct GenerateUnsubscribeURLCommand {
    #[clap(flatten)]
    pub config: ConfigOpt,
    /// Email address to generate the unsubscribe URL for
    pub email: EmailAddress,
    /// Stage of unsubscribe process
    #[clap(default_value_t, arg_enum)]
    pub stage: unsubscribe_url::UnsubscribeStage,
    /// Bypass Rate Limit
    #[clap(long)]
    pub bypass_rate_limit: bool,
}

impl GenerateUnsubscribeURLCommand {
    pub fn run(self) -> Result<()> {
        if !self.bypass_rate_limit {
            if let Err(rate_limited) = self.config.check_send_unsubscribe_email(&self.email)? {
                eprintln!("{rate_limited}");
                exit(3);
            }
        }
        let url = self.config.create_unsubscribe_url(self.email, self.stage)?;
        println!("{url}");
        Ok(())
    }
}

#[derive(Parser, Debug)]
pub struct SendUnsubscribeAuthEmailCommand {
    #[clap(flatten)]
    pub config: ConfigOpt,
    /// Email address to generate the unsubscribe URL for
    pub email: EmailAddress,
    /// Bypass Rate Limit
    #[clap(long)]
    pub bypass_rate_limit: bool,
}

impl SendUnsubscribeAuthEmailCommand {
    pub fn run(self) -> Result<()> {
        if let Err(rate_limited) = self
            .config
            .send_unsubscribe_auth_email(&self.email, self.bypass_rate_limit)?
        {
            eprintln!("{rate_limited}");
            exit(3);
        }
        Ok(())
    }
}

#[derive(Parser, Debug)]
pub struct VerifyUnsubscribeURLCommand {
    #[clap(flatten)]
    pub config: ConfigOpt,
    /// URL to verify
    pub url: Url,
}

impl VerifyUnsubscribeURLCommand {
    pub fn run(self) -> Result<()> {
        let params = self.config.verify_unsubscribe_url(self.url)?;
        println!("{params:?}");
        Ok(())
    }
}

#[derive(Parser, Debug)]
pub struct IsEmailAllowedCommand {
    #[clap(flatten)]
    pub config: ConfigOpt,
    /// Email address to check
    pub email: EmailAddress,
    /// Project to check
    pub project: String,
}

impl IsEmailAllowedCommand {
    pub fn run(self) -> Result<()> {
        ensure!(
            self.config.is_email_allowed(&self.email, &self.project)?,
            "email not allowed"
        );
        Ok(())
    }
}

#[derive(Parser, Debug)]
pub struct AllowEmailCommand {
    #[clap(flatten)]
    pub config: ConfigOpt,
    /// Email address to allow
    pub email: EmailAddress,
    /// Allow all projects
    #[clap(short, long, conflicts_with("projects"))]
    pub all: bool,
    /// Projects to allow
    #[clap(required(true))]
    pub projects: Vec<String>,
}

impl AllowEmailCommand {
    pub fn run(self) -> Result<()> {
        let mut state = self.config.get_email_state(&self.email)?;
        if self.all {
            state.allowed_projects = self.config.projects().clone();
        } else {
            state.allowed_projects.extend(self.projects);
        }
        self.config.set_email_state(&state)
    }
}

#[derive(Parser, Debug)]
pub struct DenyEmailCommand {
    #[clap(flatten)]
    pub config: ConfigOpt,
    /// Email address to deny
    pub email: EmailAddress,
    /// Deny all projects
    #[clap(short, long, conflicts_with("projects"))]
    pub all: bool,
    /// Projects to deny
    #[clap(required(true))]
    pub projects: Vec<String>,
}

impl DenyEmailCommand {
    pub fn run(self) -> Result<()> {
        let mut state = self.config.get_email_state(&self.email)?;
        if self.all {
            state.allowed_projects.clear();
        } else {
            for i in &self.projects {
                state.allowed_projects.remove(i);
            }
        }
        self.config.set_email_state(&state)
    }
}

#[derive(Parser, Debug)]
pub struct ResetUnsubscribeEmailRateLimitCommand {
    #[clap(flatten)]
    pub config: ConfigOpt,
    /// Email address to reset rate limit for
    pub email: EmailAddress,
}

impl ResetUnsubscribeEmailRateLimitCommand {
    pub fn run(self) -> Result<()> {
        self.config.reset_unsubscribe_email_rate_limit(&self.email)
    }
}

#[derive(Parser, Debug)]
#[clap(
    name = "unsubscribe",
    about = "CGI script for managing build email subscriptions"
)]
pub struct UnsubscribeCLI {
    #[clap(flatten)]
    pub config: ConfigOpt,
}

#[derive(clap::Subcommand, Debug)]
pub enum Command {
    /// Generates completion scripts for your shell
    Completions(CompletionsCommand),
    /// Generate the SSH key used for generating and checking unsubscribe urls
    GenerateKey(GenerateKeyCommand),
    /// Generate an unsubscribe URL
    GenerateUnsubscribeURL(GenerateUnsubscribeURLCommand),
    /// Verify an unsubscribe URL
    VerifyUnsubscribeURL(VerifyUnsubscribeURLCommand),
    /// Check if email address is allowed to be sent to
    IsEmailAllowed(IsEmailAllowedCommand),
    /// Add the email address to the allow-list
    AllowEmail(AllowEmailCommand),
    /// Remove the email address from the allow-list
    DenyEmail(DenyEmailCommand),
    /// Reset the unsubscribe email rate limit
    ResetUnsubscribeEmailRateLimit(ResetUnsubscribeEmailRateLimitCommand),
    /// Send email with link for changing subscriptions.
    SendUnsubscribeAuthEmail(SendUnsubscribeAuthEmailCommand),
}

#[derive(clap::Parser, Debug)]
#[clap(multicall = true)]
pub enum CLI {
    EmailManage(EmailManageCLI),
    Unsubscribe(UnsubscribeCLI),
}

fn main() -> Result<()> {
    let cli = <CLI as Parser>::parse();
    match cli {
        CLI::EmailManage(cli) => match cli.command {
            Command::Completions(cmd) => cmd.run(),
            Command::GenerateKey(cmd) => cmd.run(),
            Command::GenerateUnsubscribeURL(cmd) => cmd.run(),
            Command::VerifyUnsubscribeURL(cmd) => cmd.run(),
            Command::IsEmailAllowed(cmd) => cmd.run(),
            Command::AllowEmail(cmd) => cmd.run(),
            Command::DenyEmail(cmd) => cmd.run(),
            Command::ResetUnsubscribeEmailRateLimit(cmd) => cmd.run(),
            Command::SendUnsubscribeAuthEmail(cmd) => cmd.run(),
        },
        CLI::Unsubscribe(cli) => cli.run(),
    }
}
