use crate::{
    config::Config, email_state::UnsubscribeEmailRateLimited, unsubscribe_url::UnsubscribeStage,
};
use anyhow::{anyhow, Result};
use lettre::{EmailAddress, Envelope, SendableEmail, SendmailTransport, Transport};
use std::io::Write;

impl Config {
    pub fn send_unsubscribe_auth_email(
        &self,
        email_address: &EmailAddress,
        bypass_rate_limit: bool,
    ) -> Result<Result<(), UnsubscribeEmailRateLimited>> {
        if !bypass_rate_limit {
            match self.check_send_unsubscribe_email(email_address) {
                Ok(Ok(())) => {}
                v => return v,
            }
        }
        let link =
            self.create_unsubscribe_url(email_address.clone(), UnsubscribeStage::Authenticated)?;
        let mut message = Vec::new();
        writeln!(
            message,
            r#"Subject: Email Verification for Changing Subscription to Build Emails
To: {email_address}"#
        )?;
        if let Some(reply_to) = &self.unsubscribe_email_reply_to {
            writeln!(message, r#"Reply-To: {reply_to}"#)?;
        }
        writeln!(message)?;
        writeln!(
            message,
            r#"Your verification link is:
{link}

If you did not request this, you can safely ignore this email."#
        )?;
        let email = SendableEmail::new(
            Envelope::new(
                self.unsubscribe_email_from.clone(),
                vec![email_address.clone()],
            )?,
            String::new(),
            message,
        );
        SendmailTransport::new_with_command(
            self.sendmail_command
                .to_str()
                .ok_or_else(|| anyhow!("sendmail_command is not valid UTF-8"))?,
        )
        .send(email)?;
        Ok(Ok(()))
    }
}
