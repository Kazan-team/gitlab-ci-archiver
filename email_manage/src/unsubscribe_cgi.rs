use crate::{
    unsubscribe_url::{UnsubscribeStage, UnsubscribeUrlQueryParams},
    UnsubscribeCLI,
};
use anyhow::ensure;
use askama_escape::{escape, Html};
use cgi::{
    http::{header::LOCATION, Method, StatusCode},
    Request, Response,
};
use chrono::{DateTime, Utc};
use headers::{HeaderMapExt, RetryAfter};
use serde::Deserialize;
use std::{collections::BTreeSet, fmt::Write};

impl UnsubscribeCLI {
    fn initial_stage(
        self,
        request: Request,
        params: UnsubscribeUrlQueryParams,
    ) -> anyhow::Result<Response> {
        match *request.method() {
            Method::POST => {
                #[derive(Deserialize, Debug)]
                #[serde(deny_unknown_fields)]
                struct Form {}
                let _form: Form =
                    serde_qs::Config::new(2, false).deserialize_bytes(request.body())?;
                if let Err(e) = self
                    .config
                    .send_unsubscribe_auth_email(&params.email, false)?
                {
                    let wait_till = e.must_wait_till.to_system_time();
                    let mut response = cgi::html_response(
                        StatusCode::TOO_MANY_REQUESTS,
                        format!(
                            r#"<!DOCTYPE html>
<html>
<head>
<title>Too many emails sent in a short time</title>
</head>
<body>
Too many emails sent in a short time.
Try again after: {time}
</body>
</html>"#,
                            time = escape(&DateTime::<Utc>::from(wait_till).to_rfc2822(), Html)
                        ),
                    );
                    response
                        .headers_mut()
                        .typed_insert(RetryAfter::date(wait_till));
                    return Ok(response);
                }
                let mut response = cgi::empty_response(StatusCode::SEE_OTHER);
                response
                    .headers_mut()
                    .insert(LOCATION, request.uri().to_string().try_into()?);
                Ok(response)
            }
            Method::GET | Method::HEAD => {
                let state = self.config.get_email_state(&params.email)?;
                let mut body = String::new();
                write!(
                    body,
                    r#"<!DOCTYPE html>
<html>
<head>
<title>Verify Email to Change Subscription to Build Emails</title>
</head>
<body>
To change your subscription, you need to verify you own {email}.
<br/>
<form method="post">
<button type="submit">Send email with verification link</button>
</form>
"#,
                    email = escape(params.email.as_ref(), Html)
                )?;
                if let Some(last_time) = self.config.get_checked_last_sent_unsubscribe_time(&state)
                {
                    writeln!(
                        body,
                        r#"<br/>Last sent verification link at {last_time}"#,
                        last_time = escape(
                            &DateTime::<Utc>::from(last_time.to_system_time()).to_rfc2822(),
                            Html
                        )
                    )?;
                }
                writeln!(
                    body,
                    r#"</body>
</html>"#
                )?;
                Ok(cgi::html_response(StatusCode::OK, body))
            }
            _ => Ok(cgi::empty_response(StatusCode::METHOD_NOT_ALLOWED)),
        }
    }
    fn authenticated_stage(
        self,
        request: Request,
        params: UnsubscribeUrlQueryParams,
    ) -> anyhow::Result<Response> {
        match *request.method() {
            Method::POST => {
                #[derive(Deserialize, Debug)]
                #[serde(deny_unknown_fields)]
                struct Form {
                    #[serde(default)]
                    unsubscribe_all: bool,
                    #[serde(default)]
                    projects: BTreeSet<String>,
                }
                let form: Form =
                    serde_qs::Config::new(2, false).deserialize_bytes(request.body())?;
                ensure!(
                    form.projects.is_subset(&self.config.projects()),
                    "attempted to subscribe to non-existent project"
                );
                let mut state = self.config.get_email_state(&params.email)?;
                if form.unsubscribe_all {
                    state.allowed_projects.clear();
                } else {
                    state.allowed_projects = form.projects;
                }
                self.config.set_email_state(&state)?;
                let mut response = cgi::empty_response(StatusCode::SEE_OTHER);
                response
                    .headers_mut()
                    .insert(LOCATION, request.uri().to_string().try_into()?);
                Ok(response)
            }
            Method::GET | Method::HEAD => {
                let allowed_projects = self.config.get_email_state(&params.email)?.allowed_projects;
                let mut body = String::new();
                write!(
                    body,
                    r#"<!DOCTYPE html>
<html>
<head>
<title>Change Subscription to Build Emails</title>
</head>
<body>
Edit your subscriptions:
<form method="post">
<button type="submit">Apply Changes</button>
<br/>
<input type="checkbox" name="unsubscribe_all" value="true">Unsubscribe from all</input>
<br/>
<fieldset>
<legend>Choose which projects to subscribe to</legend>
"#
                )?;
                for project in self.config.projects() {
                    let checked;
                    if allowed_projects.contains(project) {
                        checked = " checked";
                    } else {
                        checked = "";
                    }
                    writeln!(
                        body,
                        r#"<label><input type="checkbox" {checked} name="projects[]" value="{project}"></input>{project}</label><br/>"#,
                        project = escape(project, Html)
                    )?;
                }
                writeln!(
                    body,
                    r#"</fieldset>
</form>
</body>
</html>"#
                )?;
                Ok(cgi::html_response(StatusCode::OK, body))
            }
            _ => Ok(cgi::empty_response(StatusCode::METHOD_NOT_ALLOWED)),
        }
    }
    fn cgi_main(self, request: Request) -> anyhow::Result<Response> {
        let params = match self
            .config
            .verify_unsubscribe_url_query_string(request.uri().query().unwrap_or_default())
        {
            Ok(v) => v,
            Err(e) => {
                eprintln!("{e:?}");
                return Ok(cgi::html_response(
                    StatusCode::FORBIDDEN,
                    r#"<!DOCTYPE html>
<html>
<head>
<title>Unauthorized</title>
</head>
<body>
You are unauthorized. Your unsubscribe link is either invalid or expired.
</body>
</html>"#,
                ));
            }
        };
        match params.stage {
            UnsubscribeStage::Initial => self.initial_stage(request, params),
            UnsubscribeStage::Authenticated => self.authenticated_stage(request, params),
        }
    }

    pub fn run(self) -> anyhow::Result<()> {
        cgi::handle(|request| match self.cgi_main(request) {
            Ok(v) => v,
            Err(e) => {
                eprintln!("{e:#?}");
                cgi::empty_response(500)
            }
        });
        Ok(())
    }
}
