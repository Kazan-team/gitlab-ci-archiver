use anyhow::{anyhow, ensure, Context, Result};
use clap::ArgEnum;
use core::fmt;
use ed25519_dalek::Signer;
use lettre::EmailAddress;
use rand::rngs::OsRng;
use serde::{Deserialize, Serialize};
use serde_qs;
use std::path::Path;
use tai64::{Tai64, Tai64N};
use url::Url;

use crate::config::Config;

#[derive(Serialize, Deserialize, Debug, ArgEnum, Copy, Clone, PartialEq, Eq)]
pub enum UnsubscribeStage {
    #[serde(rename = "0")]
    Initial,
    #[serde(rename = "1")]
    Authenticated,
}

impl Default for UnsubscribeStage {
    fn default() -> Self {
        Self::Initial
    }
}

pub mod sig_serde {
    use base64ct::{Base64UrlUnpadded, Encoding};
    use ed25519_dalek::Signature;
    use serde::{de::Error, Deserialize, Deserializer, Serialize, Serializer};

    pub fn serialize<S: Serializer>(sig: &Signature, ser: S) -> Result<S::Ok, S::Error> {
        Base64UrlUnpadded::encode_string(sig.as_ref()).serialize(ser)
    }

    pub fn deserialize<'de, D: Deserializer<'de>>(de: D) -> Result<Signature, D::Error> {
        let text = <&str>::deserialize(de)?;
        let bytes = Base64UrlUnpadded::decode_vec(text).map_err(D::Error::custom)?;
        Ok(Signature::from_bytes(&bytes).map_err(D::Error::custom)?)
    }
}

pub mod ts_serde {
    use serde::{Deserialize, Serialize};
    use tai64::Tai64;

    pub fn serialize<S: serde::Serializer>(ts: &Tai64, ser: S) -> Result<S::Ok, S::Error> {
        ts.0.serialize(ser)
    }

    pub fn deserialize<'de, D: serde::Deserializer<'de>>(de: D) -> Result<Tai64, D::Error> {
        Ok(Tai64(u64::deserialize(de)?))
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(transparent)]
pub struct Base64EncodedSignature(#[serde(with = "sig_serde")] pub ed25519_dalek::Signature);

#[derive(Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct UnsubscribeUrlQueryParams {
    pub email: EmailAddress,
    pub stage: UnsubscribeStage,
    #[serde(with = "ts_serde")]
    pub ts: Tai64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(default)]
    pub sig: Option<Base64EncodedSignature>,
}

pub struct Key(ed25519_dalek::Keypair);

impl fmt::Debug for Key {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        struct Hidden;
        impl fmt::Debug for Hidden {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str("<hidden>")
            }
        }

        f.debug_struct("Key")
            .field("public", &self.0.public)
            .field("private", &Hidden)
            .finish()
    }
}

impl Key {
    pub fn read_openssh_file(path: impl AsRef<Path>) -> Result<Self> {
        let path = path.as_ref();
        let key = ssh_key::PrivateKey::read_openssh_file(path)
            .with_context(|| format!("failed to read openssh key {}", path.display()))?;
        ensure!(
            !key.is_encrypted(),
            "encrypted private keys aren't supported"
        );
        let key = key
            .key_data()
            .ed25519()
            .ok_or_else(|| anyhow!("private key must be a ed25519 key"))?;
        Ok(Key(key.try_into()?))
    }

    pub fn write_openssh_file(&self, path: impl AsRef<Path>) -> Result<()> {
        let key = ssh_key::PrivateKey::from(ssh_key::private::Ed25519Keypair::from(&self.0));
        Ok(key.write_openssh_file(path.as_ref(), Default::default())?)
    }

    pub fn generate() -> Result<Self> {
        Ok(Self(ed25519_dalek::Keypair::generate(&mut OsRng {})))
    }

    pub fn create_unsubscribe_url_query_string(
        &self,
        email: EmailAddress,
        stage: UnsubscribeStage,
    ) -> Result<String> {
        let mut retval = UnsubscribeUrlQueryParams {
            email,
            stage,
            ts: Tai64::now(),
            sig: None,
        };
        let signed_query = serde_qs::to_string(&retval)?;
        retval.sig = Some(Base64EncodedSignature(self.0.sign(signed_query.as_bytes())));
        Ok(serde_qs::to_string(&retval)?)
    }

    pub fn verify_unsubscribe_url_query_string(
        &self,
        query_string: impl AsRef<[u8]>,
        valid_after: Tai64,
    ) -> Result<UnsubscribeUrlQueryParams> {
        let mut retval: UnsubscribeUrlQueryParams = serde_qs::from_bytes(query_string.as_ref())?;
        let sig = retval
            .sig
            .take()
            .ok_or_else(|| anyhow!("query string missing signature"))?;
        let signed_query = serde_qs::to_string(&retval)?;
        self.0
            .public
            .verify_strict(signed_query.as_bytes(), &sig.0)?;
        ensure!(retval.ts >= valid_after, "token expired");
        retval.sig = Some(sig);
        Ok(retval)
    }
}

impl Config {
    pub fn read_unsubscribe_url_key(&self) -> Result<Key> {
        Key::read_openssh_file(&self.unsubscribe_url_key_file)
    }
    pub fn create_unsubscribe_url(
        &self,
        email: EmailAddress,
        stage: UnsubscribeStage,
    ) -> Result<Url> {
        let mut retval = (*self.unsubscribe_url).clone();
        retval.set_query(Some(
            &self
                .read_unsubscribe_url_key()?
                .create_unsubscribe_url_query_string(email, stage)?,
        ));
        Ok(retval)
    }
    pub fn verify_unsubscribe_url_query_string(
        &self,
        query_string: impl AsRef<[u8]>,
    ) -> Result<UnsubscribeUrlQueryParams> {
        self.read_unsubscribe_url_key()?
            .verify_unsubscribe_url_query_string(
                query_string,
                (Tai64N::now() - self.unsubscribe_url_valid_duration).into(),
            )
    }
    pub fn verify_unsubscribe_url(&self, mut url: Url) -> Result<UnsubscribeUrlQueryParams> {
        let query_string = url.query().map(String::from);
        url.set_query(None);
        ensure!(url == *self.unsubscribe_url, "doesn't match expected url");
        let query_string =
            query_string.ok_or_else(|| anyhow!("unsubscribe url is missing query string"))?;
        self.verify_unsubscribe_url_query_string(query_string)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_generate_sign_verify() -> Result<()> {
        let key = dbg!(Key::generate()?);
        let query_string = dbg!(key.create_unsubscribe_url_query_string(
            "test@example.com".parse()?,
            UnsubscribeStage::Initial
        )?);
        let decoded =
            dbg!(key.verify_unsubscribe_url_query_string(query_string, Tai64::now() - 10)?);
        assert_eq!(AsRef::<str>::as_ref(&decoded.email), "test@example.com");
        Ok(())
    }
}
