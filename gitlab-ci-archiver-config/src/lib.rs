use serde::Deserialize;
use std::{fmt, fs, path::Path};

#[derive(Deserialize, Debug)]
pub struct GitLabConfig {
    pub server_host_name: String,
    #[serde(default)]
    pub insecure: bool,
    pub access_token: String,
}

#[derive(Deserialize, Debug)]
pub struct EmailConfig {
    #[serde(default)]
    pub command: Vec<String>,
}

#[derive(Deserialize, Debug)]
pub struct Config {
    pub gitlab: GitLabConfig,
    pub email: Option<EmailConfig>,
    pub projects: Vec<Project>,
    pub repo_dir: String,
}

#[derive(Deserialize, Debug)]
pub struct Project {
    pub path: String,
}

pub fn read_config(config_file_name: impl AsRef<Path>) -> Result<Config, clap::Error> {
    fn map_err(config_file_name: &Path, err: impl fmt::Display) -> clap::Error {
        clap::Error::raw(
            clap::ErrorKind::Io,
            format_args!(
                "Error reading config file \"{}\": {}\n",
                config_file_name.display(),
                err
            ),
        )
    }
    let config_file_name = config_file_name.as_ref();
    let file_contents = fs::read(config_file_name).map_err(|err| map_err(config_file_name, err))?;
    toml::from_slice(&file_contents).map_err(|err| map_err(config_file_name, err))
}

pub fn read_config_or_exit(config_file_name: impl AsRef<Path>) -> Config {
    match read_config(config_file_name) {
        Ok(v) => v,
        Err(err) => err.exit(),
    }
}
