#!/bin/bash

set -e

trap 'fail "exited with status: $?"' ERR

. send-email-config

testing=0

function fail()
{
    printf "fatal error: $@" >&2
    echo >&2
    exit 1
}

function testing_sendmail()
{
    echo -n "command"
    printf " %q" "line:" "$0" "$@"
    echo
    echo "stdin:"
    cat
}

if [[ "$1" == "test" ]]; then
    testing=1
    MAIL_PROGRAM=testing_sendmail
elif [[ "$1" != "send" ]]; then
    cat >&2 <<EOF
usage: $0 send|test
Expects to be called by gitlab-ci-archiver, which sets a bunch of required
environment variables.
EOF
    exit 1
fi

if [[ -z "$GITLAB_CI_ARCHIVER_ARCHIVE_COMMIT" ]]; then
    echo "missing GITLAB_CI_ARCHIVER_* environment variables!" >&2
    if ((testing)); then
        echo "run tests using test-send-email.sh" >&2
    fi
fi

declare -A sent_emails

function email_body()
{
    local unsubscribe_link="$1"
    echo "To see the full build log, run:"
    echo "git clone \"$ARCHIVE_REPO_PUBLIC_URL\" build-archive"
    echo "cd build-archive"
    echo "git checkout $GITLAB_CI_ARCHIVER_ARCHIVE_COMMIT"
    echo "less -R $GITLAB_CI_ARCHIVER_ARCHIVE_LOG_FILE"
    echo ""
    echo "Pipeline on GitLab: $GITLAB_CI_ARCHIVER_PIPELINE_WEB_URL"
    echo ""
    echo "Truncated Build Log:"
    tail -n "$BUILD_LOG_TAIL_COUNT"
    echo
    echo "Unsubscribe link:"
    echo "$unsubscribe_link"
}

function send_email()
{
    local email_without_name="$1" name="$2"
    if ! ./email-manage is-email-allowed "$email_without_name" "$GITLAB_CI_ARCHIVER_PROJECT_PATH_WITH_NAMESPACE"; then
        echo "email not in allow list, not sending: \"$email_without_name\""
        return
    fi
    if [[ "${sent_emails["$email_without_name"]}" != 1 ]]; then
        sent_emails["$email_without_name"]=1
    else
        echo "email already sent, not sending duplicate: \"$email_without_name\""
        return
    fi
    local email="$email_without_name"
    if [[ "$name" != "" ]]; then
        email="$name <$email_without_name>"
    fi
    local subject=""
    subject+="Build $GITLAB_CI_ARCHIVER_PIPELINE_STATUS: "
    subject+="$GITLAB_CI_ARCHIVER_PROJECT_NAME_WITH_NAMESPACE "
    subject+="$GITLAB_CI_ARCHIVER_PIPELINE_REF: "
    subject+="Commit ${GITLAB_CI_ARCHIVER_PIPELINE_SHA:0:7}: "
    subject+="${GITLAB_CI_ARCHIVER_COMMIT_TITLE:0:200}"$'\n'
    [[ "$subject" =~ ^([^$'\r\n']*).*$ ]] || fail "invalid Subject field: %s" "$subject"
    subject="${BASH_REMATCH[1]}"
    [[ "$EMAIL_FROM" =~ ^[^$'\r\n']*$ ]] || fail "invalid From field: %s" "$EMAIL_FROM"
    [[ "$EMAIL_REPLY_TO" =~ ^[^$'\r\n']*$ ]] || fail "invalid Reply-To field: %s" "$EMAIL_REPLY_TO"
    local headers=""
    headers+="Content-Type: text/plain; charset=utf-8"$'\n'
    if [[ "$EMAIL_FROM" != "" ]]; then
        headers+="From: $EMAIL_FROM"$'\n'
    fi
    if [[ "$EMAIL_REPLY_TO" != "" ]]; then
        headers+="Reply-To: $EMAIL_REPLY_TO"$'\n'
    fi
    headers+="To: $email"$'\n'
    headers+="Subject: $subject"$'\n'
    local body unsubscribe_link
    unsubscribe_link="$(./email-manage generate-unsubscribe-url --bypass-rate-limit "$email_without_name")"
    mapfile body < <(email_body "$unsubscribe_link")
    {
        echo "$headers"
        printf "%s" "${body[@]}"
    } | "$MAIL_PROGRAM" -i -B8BITMIME -- "$email"
}

case "$GITLAB_CI_ARCHIVER_PIPELINE_STATUS" in
    success)
        for email in "${MAILING_LISTS_FOR_SUCCESSES[@]}"; do
            send_email "$email"
        done
    ;;
    canceled)
        for email in "${MAILING_LISTS_FOR_CANCELS[@]}"; do
            send_email "$email"
        done
    ;;
    failed)
        send_email "$GITLAB_CI_ARCHIVER_COMMIT_AUTHOR_EMAIL" "$GITLAB_CI_ARCHIVER_COMMIT_AUTHOR_NAME"
        send_email "$GITLAB_CI_ARCHIVER_COMMIT_COMMITTER_EMAIL" "$GITLAB_CI_ARCHIVER_COMMIT_COMMITTER_NAME"
        for email in "${MAILING_LISTS_FOR_FAILURES[@]}"; do
            send_email "$email"
        done
    ;;
    *)
        fail "unknown pipeline status: %s" "$GITLAB_CI_ARCHIVER_PIPELINE_STATUS"
    ;;
esac
