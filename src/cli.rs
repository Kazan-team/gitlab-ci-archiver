use clap::IntoApp;
use clap_complete::{generate, Shell};
use gitlab_ci_archiver_config::{read_config_or_exit, Config};
use std::io::stdout;

#[derive(clap::Parser, Debug)]
#[clap(
    name = "gitlab-ci-archiver",
    about = "Archives GitLab CI Build logs in a Git repository"
)]
pub struct CLI {
    #[clap(subcommand)]
    pub cmd: Command,
}

#[derive(clap::Parser, Debug)]
pub struct CompletionsCommand {
    shell: Shell,
}

impl CompletionsCommand {
    pub fn run(self) -> Result<(), String> {
        generate(
            self.shell,
            &mut CLI::command(),
            "gitlab-ci-archiver",
            &mut stdout(),
        );
        Ok(())
    }
}

#[derive(clap::Parser, Debug)]
pub struct PollCommand {
    /// The config file path
    #[clap(short, long, parse(from_os_str = read_config_or_exit))]
    pub config: Config,
    /// If a full sync should be done
    #[clap(short, long)]
    pub full: bool,
}

#[derive(clap::Subcommand, Debug)]
pub enum Command {
    /// Generates completion scripts for your shell
    Completions(CompletionsCommand),
    /// Poll the GitLab server for the latest completed builds
    Poll(PollCommand),
}
