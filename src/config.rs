use gitlab::Gitlab;
use gitlab_ci_archiver_config::GitLabConfig;

pub fn create_gitlab(gitlab_config: &GitLabConfig) -> Result<Gitlab, String> {
    if gitlab_config.insecure {
        Gitlab::new_insecure(&gitlab_config.server_host_name, &gitlab_config.access_token)
    } else {
        Gitlab::new(&gitlab_config.server_host_name, &gitlab_config.access_token)
    }
    .map_err(|e| e.to_string())
}
