use gitlab_ci_archiver_config::EmailConfig;
use serde::Serialize;
use serde_json::Value;
use std::{
    borrow::Cow,
    io::Write,
    mem,
    process::{Command, Stdio},
};

#[derive(Serialize)]
struct EmailEnvironmentVariables<'a> {
    archive_commit: &'a str,
    archive_log_file: &'a str,
    project: &'a gitlab::Project,
    job: &'a gitlab::Job,
    pipeline: &'a gitlab::Pipeline,
    commit: &'a gitlab::RepoCommitDetail,
}

struct EnvironmentTranslator {
    env_vars: Vec<(String, String)>,
}

impl EnvironmentTranslator {
    fn add_env_var(&mut self, name: impl Into<String>, value: impl Into<String>) {
        let mut name = name.into();
        name.make_ascii_uppercase();
        self.env_vars.push((name.into(), value.into()));
    }
    fn translate_from_json(&mut self, prefix: Cow<str>, value: &Value) {
        match *value {
            Value::Null => self.add_env_var(prefix, "null"),
            Value::Bool(v) => self.add_env_var(prefix, v.to_string()),
            Value::Number(ref v) => self.add_env_var(prefix, v.to_string()),
            Value::String(ref v) => self.add_env_var(prefix, v.to_string()),
            Value::Array(ref values) => {
                for (i, v) in values.iter().enumerate() {
                    self.translate_from_json(Cow::Owned(format!("{}_{}", prefix, i)), v);
                }
                self.add_env_var(prefix.into_owned() + "_len", values.len().to_string());
            }
            Value::Object(ref values) => {
                for (k, v) in values.iter() {
                    self.translate_from_json(Cow::Owned(format!("{}_{}", prefix, k)), v);
                }
            }
        }
    }
    fn new() -> Self {
        Self { env_vars: vec![] }
    }
}

pub fn send_email(
    email_config: &EmailConfig,
    archive_commit: &str,
    archive_log_file: &str,
    project: &gitlab::Project,
    job: &gitlab::Job,
    pipeline: &gitlab::Pipeline,
    commit: &gitlab::RepoCommitDetail,
    log: &[u8],
) -> Result<(), String> {
    let email_environment_variables = serde_json::to_value(EmailEnvironmentVariables {
        archive_commit,
        archive_log_file,
        project,
        job,
        pipeline,
        commit,
    })
    .map_err(|e| e.to_string())?;
    let mut environment = EnvironmentTranslator::new();
    environment.translate_from_json(
        Cow::Borrowed("GITLAB_CI_ARCHIVER"),
        &email_environment_variables,
    );
    if let Some((program, args)) = email_config.command.split_first() {
        let mut child = Command::new(program)
            .args(args)
            .envs(environment.env_vars)
            .stdin(Stdio::piped())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .spawn()
            .map_err(|e| format!("failed to execute {:?}: {}", program, e))?;
        let mut stdin = child.stdin.take().expect("known to be Some");
        if let Err(e) = stdin.write_all(log) {
            eprintln!("Error writing to child process's stdin: {}", e);
        }
        mem::drop(stdin);
        let exit_status = child.wait().map_err(|e| e.to_string())?;
        if !exit_status.success() {
            eprintln!("child process exited with status {}", exit_status);
        }
        Ok(())
    } else {
        println!("set env vars:");
        for (k, v) in environment.env_vars {
            println!("{} = {:?}", k, v);
        }
        println!("no email command");
        Ok(())
    }
}
