use git2::{build::TreeUpdateBuilder, Commit, ErrorCode, Oid, Repository, RepositoryState, Tree};
use gitlab::{JobId, PipelineId};
use std::{borrow::Cow, fmt, path::Path, process::Command, slice};

#[derive(Clone, Debug)]
pub struct Job<'a> {
    pub job_id: JobId,
    pub job_info: Cow<'a, str>,
    pub pipeline_id: PipelineId,
    pub pipeline_info: Cow<'a, str>,
    pub log: Cow<'a, [u8]>,
}

pub struct GitRepo {
    repo: Repository,
}

fn verify_repo(repo: &Repository) -> Result<(), String> {
    if !repo.is_bare() {
        return Err("repo must be bare".into());
    }
    if repo.state() != RepositoryState::Clean {
        return Err("repo state is not clean".into());
    }
    if repo.head_detached().map_err(|e| e.to_string())? {
        return Err("repo HEAD is detached; you need to checkout a branch".into());
    }
    Ok(())
}

fn make_path(id: PipelineId, file_name: fmt::Arguments<'_>) -> String {
    format!("pipelines/{id}/{file_name}")
}

struct GitRepoError(String);

impl From<GitRepoError> for String {
    fn from(v: GitRepoError) -> Self {
        v.0
    }
}

impl From<git2::Error> for GitRepoError {
    fn from(v: git2::Error) -> Self {
        Self(v.to_string())
    }
}

impl From<std::io::Error> for GitRepoError {
    fn from(v: std::io::Error) -> Self {
        Self(v.to_string())
    }
}

impl From<String> for GitRepoError {
    fn from(v: String) -> Self {
        Self(v)
    }
}

impl GitRepo {
    pub fn open(path: impl AsRef<Path>) -> Result<Self, String> {
        let repo = Repository::open(path).map_err(|v| v.to_string())?;
        verify_repo(&repo)?;
        Ok(Self { repo })
    }
    fn get_pipeline_info_path(id: PipelineId) -> String {
        make_path(id, format_args!("info.txt"))
    }
    fn get_pipeline_job_info_path(pipeline_id: PipelineId, job_id: JobId) -> String {
        make_path(pipeline_id, format_args!("job-{job_id}-info.txt"))
    }
    pub fn get_pipeline_job_log_path(&self, pipeline_id: PipelineId, job_id: JobId) -> String {
        make_path(pipeline_id, format_args!("job-{job_id}-log.txt"))
    }
    fn get_head_commit_and_tree(&self) -> Result<(Option<Commit>, Option<Tree>), GitRepoError> {
        match self.repo.head() {
            Ok(head) => {
                let commit = head.peel_to_commit()?;
                let tree = commit.tree()?;
                Ok((Some(commit), Some(tree)))
            }
            Err(ref e) if e.code() == ErrorCode::UnbornBranch => Ok((None, None)),
            Err(e) => Err(e.into()),
        }
    }
    fn has_job_in_tree(
        pipeline_id: PipelineId,
        job_id: JobId,
        tree: Option<&Tree>,
    ) -> Result<bool, GitRepoError> {
        if let Some(tree) = tree {
            match tree.get_path(Self::get_pipeline_job_info_path(pipeline_id, job_id).as_ref()) {
                Ok(_) => Ok(true),
                Err(ref e) if e.code() == ErrorCode::NotFound => Ok(false),
                Err(e) => Err(e.into()),
            }
        } else {
            Ok(false)
        }
    }
    pub fn has_job(&self, pipeline_id: PipelineId, job_id: JobId) -> Result<bool, String> {
        let (_, tree) = self.get_head_commit_and_tree()?;
        Ok(Self::has_job_in_tree(pipeline_id, job_id, tree.as_ref())?)
    }
    fn add_job_helper(&self, job: Job<'_>, commit_message: String) -> Result<Oid, GitRepoError> {
        let Job {
            job_id,
            job_info,
            pipeline_id,
            pipeline_info,
            log,
        } = job;
        let (parent_commit, parent_tree) = self.get_head_commit_and_tree()?;
        let parent_tree = match parent_tree {
            Some(parent_tree) => parent_tree,
            None => self.repo.find_tree(self.repo.treebuilder(None)?.write()?)?,
        };
        let mut tree_update_builder = TreeUpdateBuilder::new();
        tree_update_builder.upsert(
            Self::get_pipeline_info_path(pipeline_id),
            self.repo.blob(pipeline_info.as_bytes())?,
            git2::FileMode::Blob,
        );
        tree_update_builder.upsert(
            Self::get_pipeline_job_info_path(pipeline_id, job_id),
            self.repo.blob(job_info.as_bytes())?,
            git2::FileMode::Blob,
        );
        tree_update_builder.upsert(
            self.get_pipeline_job_log_path(pipeline_id, job_id),
            self.repo.blob(&log)?,
            git2::FileMode::Blob,
        );
        let tree = tree_update_builder.create_updated(&self.repo, &parent_tree)?;
        let tree = self.repo.find_tree(tree)?;
        let signature = self.repo.signature()?;
        let retval = self.repo.commit(
            Some("HEAD"),
            &signature,
            &signature,
            &commit_message,
            &tree,
            parent_commit.as_ref().as_ref().map_or(&[], slice::from_ref),
        )?;

        // libgit2 doesn't implement gc, so run git gc ourselves
        let status = Command::new("git")
            .args(["gc", "--auto"])
            .current_dir(self.repo.path())
            .status()?;
        if !status.success() {
            return Err(format!("`git gc --auto` failed: {status}").into());
        }

        // run git update-server-info
        let status = Command::new("git")
            .args(["update-server-info"])
            .current_dir(self.repo.path())
            .status()?;
        if !status.success() {
            return Err(format!("`git update-server-info` failed: {status}").into());
        }

        Ok(retval)
    }
    pub fn add_job(&self, job: Job<'_>, commit_message: String) -> Result<Oid, String> {
        Ok(self.add_job_helper(job, commit_message)?)
    }
}
