use clap::Parser;
use std::process::exit;

mod cli;
mod config;
mod email;
mod git_repo;
mod poll_command;

fn main() {
    let cli = cli::CLI::parse();
    let result = match cli.cmd {
        cli::Command::Completions(cmd) => cmd.run(),
        cli::Command::Poll(cmd) => cmd.run(),
    };
    if let Err(err) = result {
        eprintln!("error: {}", err);
        exit(1);
    }
}
