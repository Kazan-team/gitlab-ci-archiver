use crate::{
    cli::PollCommand,
    config::create_gitlab,
    email::send_email,
    git_repo::{self, GitRepo},
};
use gitlab::{
    api::{self, projects::jobs::JobScope, Pagination, Query},
    types,
};
use std::{borrow::Cow, fmt::Write, thread::sleep, time::Duration};

fn map_err(v: impl ToString) -> String {
    v.to_string()
}

impl PollCommand {
    pub fn run(self) -> Result<(), String> {
        let gitlab_ci_archiver_config::Config {
            gitlab,
            email,
            projects,
            repo_dir,
        } = self.config;
        let git_repo = GitRepo::open(repo_dir)?;
        let gitlab = create_gitlab(&gitlab)?;
        for gitlab_ci_archiver_config::Project { path } in &projects {
            let mut project: types::Project = api::projects::Project::builder()
                .project(&**path)
                .build()
                .unwrap()
                .query(&gitlab)
                .map_err(map_err)?;

            project.runners_token = None; // hide secret values

            let pagination = if self.full {
                Pagination::All
            } else {
                Pagination::Limit(100)
            };
            for job in api::paged(
                api::projects::jobs::Jobs::builder()
                    .project(project.id.value())
                    .scopes([JobScope::Failed, JobScope::Success, JobScope::Canceled].into_iter())
                    .build()
                    .unwrap(),
                pagination,
            )
            .iter(&gitlab)
            {
                let job: types::Job = job.map_err(map_err)?;
                if git_repo.has_job(job.pipeline.id, job.id)? {
                    continue;
                }
                println!("found new job:\n{:#?}", job);
                sleep(Duration::from_secs(2));
                let pipeline: types::Pipeline = api::projects::pipelines::Pipeline::builder()
                    .project(project.id.value())
                    .pipeline(job.pipeline.id.value())
                    .build()
                    .unwrap()
                    .query(&gitlab)
                    .map_err(map_err)?;
                println!("job pipeline:\n{:#?}", pipeline);
                let commit: types::RepoCommitDetail =
                    api::projects::repository::commits::Commit::builder()
                        .project(project.id.value())
                        .commit(&**pipeline.sha.value())
                        .build()
                        .unwrap()
                        .query(&gitlab)
                        .map_err(map_err)?;
                println!("job commit:\n{:#?}", commit);
                let log: Vec<u8> = api::raw(
                    api::projects::jobs::JobTrace::builder()
                        .project(project.id.value())
                        .job(job.id.value())
                        .build()
                        .unwrap(),
                )
                .query(&gitlab)
                .map_err(map_err)?;
                let mut commit_message = String::new();
                writeln!(
                    commit_message,
                    "Build {status:?}: {job_url}",
                    status = job.status,
                    job_url = job.web_url,
                )
                .map_err(map_err)?;
                writeln!(commit_message).map_err(map_err)?;
                if let Some(branch) = &pipeline.ref_ {
                    writeln!(commit_message, "Branch: {}", branch).map_err(map_err)?;
                }
                writeln!(commit_message, "Commit: {}", pipeline.sha.value()).map_err(map_err)?;
                writeln!(
                    commit_message,
                    "Author: {} <{}>",
                    commit.author_name, commit.author_email
                )
                .map_err(map_err)?;
                writeln!(
                    commit_message,
                    "Committer: {} <{}>",
                    commit.committer_name, commit.committer_email
                )
                .map_err(map_err)?;
                write!(commit_message, "Message:\n{}", commit.message).map_err(map_err)?;
                let archive_commit = git_repo.add_job(
                    git_repo::Job {
                        job_id: job.id,
                        job_info: Cow::Owned(serde_json::to_string_pretty(&job).map_err(map_err)?),
                        pipeline_id: job.pipeline.id,
                        pipeline_info: Cow::Owned(
                            serde_json::to_string_pretty(&pipeline).map_err(map_err)?,
                        ),
                        log: Cow::Borrowed(&log),
                    },
                    commit_message,
                )?;
                if let Some(email) = &email {
                    send_email(
                        email,
                        &archive_commit.to_string(),
                        &git_repo.get_pipeline_job_log_path(job.pipeline.id, job.id),
                        &project,
                        &job,
                        &pipeline,
                        &commit,
                        &log,
                    )?;
                }
            }
        }
        Ok(())
    }
}
