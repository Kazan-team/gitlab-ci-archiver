#!/bin/bash

export GITLAB_CI_ARCHIVER_ARCHIVE_COMMIT='bdcaae832affd59c6b7443516d760a5243abcefb'
export GITLAB_CI_ARCHIVER_ARCHIVE_LOG_FILE='pipelines/128199/job-684486-log.txt'
export GITLAB_CI_ARCHIVER_COMMIT_AUTHORED_DATE='2020-04-21T00:21:03Z'
export GITLAB_CI_ARCHIVER_COMMIT_AUTHOR_EMAIL='programmerjake@gmail.com'
export GITLAB_CI_ARCHIVER_COMMIT_AUTHOR_NAME='Jacob Lifshay'
export GITLAB_CI_ARCHIVER_COMMIT_COMMITTED_DATE='2020-04-21T00:21:03Z'
export GITLAB_CI_ARCHIVER_COMMIT_COMMITTER_EMAIL='programmerjake@gmail.com'
export GITLAB_CI_ARCHIVER_COMMIT_COMMITTER_NAME='Jacob Lifshay'
export GITLAB_CI_ARCHIVER_COMMIT_CREATED_AT='2020-04-21T00:21:03Z'
export GITLAB_CI_ARCHIVER_COMMIT_ID='d557f4fb2a9e5b159c7abbdcd1d85712fabc6b9b'
export GITLAB_CI_ARCHIVER_COMMIT_LAST_PIPELINE_CREATED_AT='2020-04-21T00:22:07.910Z'
export GITLAB_CI_ARCHIVER_COMMIT_LAST_PIPELINE_ID='128199'
export GITLAB_CI_ARCHIVER_COMMIT_LAST_PIPELINE_PROJECT_ID='44798'
export GITLAB_CI_ARCHIVER_COMMIT_LAST_PIPELINE_REF='master'
export GITLAB_CI_ARCHIVER_COMMIT_LAST_PIPELINE_SHA='d557f4fb2a9e5b159c7abbdcd1d85712fabc6b9b'
export GITLAB_CI_ARCHIVER_COMMIT_LAST_PIPELINE_STATUS='failed'
export GITLAB_CI_ARCHIVER_COMMIT_LAST_PIPELINE_UPDATED_AT='2020-04-21T00:31:58.262Z'
export GITLAB_CI_ARCHIVER_COMMIT_LAST_PIPELINE_WEB_URL='https://salsa.debian.org/Kazan-team/mirrors/soc/-/pipelines/128199'
export GITLAB_CI_ARCHIVER_COMMIT_MESSAGE='add libmpfr-dev to .gitlab-ci.yml'
export GITLAB_CI_ARCHIVER_COMMIT_PARENT_IDS_0='076eaaf0f52dcd0f7ffcf74aab7f1365fd9ae240'
export GITLAB_CI_ARCHIVER_COMMIT_PARENT_IDS_LEN='1'
export GITLAB_CI_ARCHIVER_COMMIT_PROJECT_ID='44798'
export GITLAB_CI_ARCHIVER_COMMIT_SHORT_ID='d557f4fb'
export GITLAB_CI_ARCHIVER_COMMIT_STATS_ADDITIONS='1'
export GITLAB_CI_ARCHIVER_COMMIT_STATS_DELETIONS='1'
export GITLAB_CI_ARCHIVER_COMMIT_STATS_TOTAL='2'
export GITLAB_CI_ARCHIVER_COMMIT_STATUS='failed'
export GITLAB_CI_ARCHIVER_COMMIT_TITLE='add libmpfr-dev to .gitlab-ci.yml'
export GITLAB_CI_ARCHIVER_JOB_ALLOW_FAILURE='false'
export GITLAB_CI_ARCHIVER_JOB_ARTIFACTS_0_FILENAME='job.log'
export GITLAB_CI_ARCHIVER_JOB_ARTIFACTS_0_FILE_FORMAT='null'
export GITLAB_CI_ARCHIVER_JOB_ARTIFACTS_0_FILE_TYPE='trace'
export GITLAB_CI_ARCHIVER_JOB_ARTIFACTS_0_SIZE='1460651'
export GITLAB_CI_ARCHIVER_JOB_ARTIFACTS_EXPIRE_AT='null'
export GITLAB_CI_ARCHIVER_JOB_ARTIFACTS_FILE='null'
export GITLAB_CI_ARCHIVER_JOB_ARTIFACTS_LEN='1'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_AUTHORED_DATE='2020-04-21T00:21:03Z'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_AUTHOR_EMAIL='programmerjake@gmail.com'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_AUTHOR_NAME='Jacob Lifshay'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_COMMITTED_DATE='2020-04-21T00:21:03Z'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_COMMITTER_EMAIL='programmerjake@gmail.com'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_COMMITTER_NAME='Jacob Lifshay'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_CREATED_AT='2020-04-21T00:21:03Z'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_ID='d557f4fb2a9e5b159c7abbdcd1d85712fabc6b9b'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_MESSAGE='add libmpfr-dev to .gitlab-ci.yml'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_PARENT_IDS_0='076eaaf0f52dcd0f7ffcf74aab7f1365fd9ae240'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_PARENT_IDS_LEN='1'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_SHORT_ID='d557f4fb'
export GITLAB_CI_ARCHIVER_JOB_COMMIT_TITLE='add libmpfr-dev to .gitlab-ci.yml'
export GITLAB_CI_ARCHIVER_JOB_COVERAGE='null'
export GITLAB_CI_ARCHIVER_JOB_CREATED_AT='2020-04-21T00:22:07.946Z'
export GITLAB_CI_ARCHIVER_JOB_DURATION='587.407089'
export GITLAB_CI_ARCHIVER_JOB_FINISHED_AT='2020-04-21T00:31:58.134Z'
export GITLAB_CI_ARCHIVER_JOB_ID='684486'
export GITLAB_CI_ARCHIVER_JOB_NAME='build'
export GITLAB_CI_ARCHIVER_JOB_PIPELINE_CREATED_AT='2020-04-21T00:22:07.910Z'
export GITLAB_CI_ARCHIVER_JOB_PIPELINE_ID='128199'
export GITLAB_CI_ARCHIVER_JOB_PIPELINE_PROJECT_ID='44798'
export GITLAB_CI_ARCHIVER_JOB_PIPELINE_REF='master'
export GITLAB_CI_ARCHIVER_JOB_PIPELINE_SHA='d557f4fb2a9e5b159c7abbdcd1d85712fabc6b9b'
export GITLAB_CI_ARCHIVER_JOB_PIPELINE_STATUS='failed'
export GITLAB_CI_ARCHIVER_JOB_PIPELINE_UPDATED_AT='2020-04-21T00:31:58.262Z'
export GITLAB_CI_ARCHIVER_JOB_PIPELINE_WEB_URL='https://salsa.debian.org/Kazan-team/mirrors/soc/-/pipelines/128199'
export GITLAB_CI_ARCHIVER_JOB_REF='master'
export GITLAB_CI_ARCHIVER_JOB_RUNNER_ACTIVE='true'
export GITLAB_CI_ARCHIVER_JOB_RUNNER_DESCRIPTION='jacob-build-server'
export GITLAB_CI_ARCHIVER_JOB_RUNNER_ID='147'
export GITLAB_CI_ARCHIVER_JOB_RUNNER_IS_SHARED='false'
export GITLAB_CI_ARCHIVER_JOB_RUNNER_NAME='gitlab-runner'
export GITLAB_CI_ARCHIVER_JOB_STAGE='build'
export GITLAB_CI_ARCHIVER_JOB_STARTED_AT='2020-04-21T00:22:10.727Z'
export GITLAB_CI_ARCHIVER_JOB_STATUS='failed'
export GITLAB_CI_ARCHIVER_JOB_TAG='false'
export GITLAB_CI_ARCHIVER_JOB_USER_AVATAR_URL='https://seccdn.libravatar.org/avatar/7a6a8c9e5f836f6874969a8a59801fb6?s=80&d=identicon&gravatarproxy=n'
export GITLAB_CI_ARCHIVER_JOB_USER_BIO=''
export GITLAB_CI_ARCHIVER_JOB_USER_CREATED_AT='2018-10-23T21:02:21.033Z'
export GITLAB_CI_ARCHIVER_JOB_USER_HIGHEST_ROLE='null'
export GITLAB_CI_ARCHIVER_JOB_USER_ID='4124'
export GITLAB_CI_ARCHIVER_JOB_USER_IS_ADMIN='null'
export GITLAB_CI_ARCHIVER_JOB_USER_LINKEDIN=''
export GITLAB_CI_ARCHIVER_JOB_USER_LOCATION='null'
export GITLAB_CI_ARCHIVER_JOB_USER_NAME='Jacob Lifshay'
export GITLAB_CI_ARCHIVER_JOB_USER_ORGANIZATION='null'
export GITLAB_CI_ARCHIVER_JOB_USER_PRIVATE_PROFILE='null'
export GITLAB_CI_ARCHIVER_JOB_USER_PUBLIC_EMAIL=''
export GITLAB_CI_ARCHIVER_JOB_USER_SKYPE=''
export GITLAB_CI_ARCHIVER_JOB_USER_STATE='active'
export GITLAB_CI_ARCHIVER_JOB_USER_TWITTER=''
export GITLAB_CI_ARCHIVER_JOB_USER_USERNAME='programmerjake-guest'
export GITLAB_CI_ARCHIVER_JOB_USER_WEBSITE_URL=''
export GITLAB_CI_ARCHIVER_JOB_USER_WEB_URL='https://salsa.debian.org/programmerjake-guest'
export GITLAB_CI_ARCHIVER_JOB_WEB_URL='https://salsa.debian.org/Kazan-team/mirrors/soc/-/jobs/684486'
export GITLAB_CI_ARCHIVER_PIPELINE_BEFORE_SHA='076eaaf0f52dcd0f7ffcf74aab7f1365fd9ae240'
export GITLAB_CI_ARCHIVER_PIPELINE_COMMITTED_AT='null'
export GITLAB_CI_ARCHIVER_PIPELINE_COVERAGE='null'
export GITLAB_CI_ARCHIVER_PIPELINE_CREATED_AT='2020-04-21T00:22:07.910Z'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_DETAILS_PATH='/Kazan-team/mirrors/soc/-/pipelines/128199'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_FAVICON='/assets/ci_favicons/favicon_status_failed-41304d7f7e3828808b0c26771f0309e55296819a9beea3ea9fbf6689d9857c12.png'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_GROUP='failed'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_HAS_DETAILS='true'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_ICON='status_failed'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_ILLUSTRATION='null'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_LABEL='failed'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_TEXT='failed'
export GITLAB_CI_ARCHIVER_PIPELINE_DETAILED_STATUS_TOOLTIP='failed'
export GITLAB_CI_ARCHIVER_PIPELINE_DURATION='587'
export GITLAB_CI_ARCHIVER_PIPELINE_FINISHED_AT='2020-04-21T00:31:58.253Z'
export GITLAB_CI_ARCHIVER_PIPELINE_ID='128199'
export GITLAB_CI_ARCHIVER_PIPELINE_PROJECT_ID='44798'
export GITLAB_CI_ARCHIVER_PIPELINE_REF='master'
export GITLAB_CI_ARCHIVER_PIPELINE_SHA='d557f4fb2a9e5b159c7abbdcd1d85712fabc6b9b'
export GITLAB_CI_ARCHIVER_PIPELINE_STARTED_AT='2020-04-21T00:22:10.810Z'
export GITLAB_CI_ARCHIVER_PIPELINE_STATUS='failed'
export GITLAB_CI_ARCHIVER_PIPELINE_TAG='false'
export GITLAB_CI_ARCHIVER_PIPELINE_UPDATED_AT='2020-04-21T00:31:58.262Z'
export GITLAB_CI_ARCHIVER_PIPELINE_USER_AVATAR_URL='https://seccdn.libravatar.org/avatar/7a6a8c9e5f836f6874969a8a59801fb6?s=80&d=identicon&gravatarproxy=n'
export GITLAB_CI_ARCHIVER_PIPELINE_USER_ID='4124'
export GITLAB_CI_ARCHIVER_PIPELINE_USER_NAME='Jacob Lifshay'
export GITLAB_CI_ARCHIVER_PIPELINE_USER_STATE='active'
export GITLAB_CI_ARCHIVER_PIPELINE_USER_USERNAME='programmerjake-guest'
export GITLAB_CI_ARCHIVER_PIPELINE_USER_WEB_URL='https://salsa.debian.org/programmerjake-guest'
export GITLAB_CI_ARCHIVER_PIPELINE_WEB_URL='https://salsa.debian.org/Kazan-team/mirrors/soc/-/pipelines/128199'
export GITLAB_CI_ARCHIVER_PIPELINE_YAML_ERRORS='null'
export GITLAB_CI_ARCHIVER_PROJECT_ARCHIVED='false'
export GITLAB_CI_ARCHIVER_PROJECT_AVATAR_URL='null'
export GITLAB_CI_ARCHIVER_PROJECT_BUILDS_ACCESS_LEVEL='enabled'
export GITLAB_CI_ARCHIVER_PROJECT_CI_CONFIG_PATH='null'
export GITLAB_CI_ARCHIVER_PROJECT_CONTAINER_REGISTRY_ENABLED='true'
export GITLAB_CI_ARCHIVER_PROJECT_CREATED_AT='2020-03-26T00:09:08.128Z'
export GITLAB_CI_ARCHIVER_PROJECT_CREATOR_ID='4124'
export GITLAB_CI_ARCHIVER_PROJECT_DEFAULT_BRANCH='master'
export GITLAB_CI_ARCHIVER_PROJECT_DESCRIPTION='mirror of https://git.libre-soc.org/git/soc.git'
export GITLAB_CI_ARCHIVER_PROJECT_EMPTY_REPO='false'
export GITLAB_CI_ARCHIVER_PROJECT_FORKED_FROM_PROJECT='null'
export GITLAB_CI_ARCHIVER_PROJECT_FORKS_COUNT='0'
export GITLAB_CI_ARCHIVER_PROJECT_HTTP_URL_TO_REPO='https://salsa.debian.org/Kazan-team/mirrors/soc.git'
export GITLAB_CI_ARCHIVER_PROJECT_ID='44798'
export GITLAB_CI_ARCHIVER_PROJECT_IMPORT_ERROR='null'
export GITLAB_CI_ARCHIVER_PROJECT_ISSUES_ACCESS_LEVEL='disabled'
export GITLAB_CI_ARCHIVER_PROJECT_ISSUES_ENABLED='false'
export GITLAB_CI_ARCHIVER_PROJECT_JOBS_ENABLED='true'
export GITLAB_CI_ARCHIVER_PROJECT_LAST_ACTIVITY_AT='2022-03-30T09:33:43.502Z'
export GITLAB_CI_ARCHIVER_PROJECT_LFS_ENABLED='true'
export GITLAB_CI_ARCHIVER_PROJECT_MERGE_METHOD='merge'
export GITLAB_CI_ARCHIVER_PROJECT_MERGE_REQUESTS_ACCESS_LEVEL='enabled'
export GITLAB_CI_ARCHIVER_PROJECT_MERGE_REQUESTS_ENABLED='true'
export GITLAB_CI_ARCHIVER_PROJECT_NAME='soc'
export GITLAB_CI_ARCHIVER_PROJECT_NAMESPACE_AVATAR_URL='null'
export GITLAB_CI_ARCHIVER_PROJECT_NAMESPACE_FULL_PATH='Kazan-team/mirrors'
export GITLAB_CI_ARCHIVER_PROJECT_NAMESPACE_ID='13770'
export GITLAB_CI_ARCHIVER_PROJECT_NAMESPACE_KIND='group'
export GITLAB_CI_ARCHIVER_PROJECT_NAMESPACE_MEMBERS_COUNT_WITH_DESCENDANTS='null'
export GITLAB_CI_ARCHIVER_PROJECT_NAMESPACE_NAME='mirrors'
export GITLAB_CI_ARCHIVER_PROJECT_NAMESPACE_PATH='mirrors'
export GITLAB_CI_ARCHIVER_PROJECT_NAMESPACE_WEB_URL='https://salsa.debian.org/groups/Kazan-team/mirrors'
export GITLAB_CI_ARCHIVER_PROJECT_NAME_WITH_NAMESPACE='Kazan / mirrors / soc'
export GITLAB_CI_ARCHIVER_PROJECT_ONLY_ALLOW_MERGE_IF_ALL_DISCUSSIONS_ARE_RESOLVED='false'
export GITLAB_CI_ARCHIVER_PROJECT_ONLY_ALLOW_MERGE_IF_PIPELINE_SUCCEEDS='false'
export GITLAB_CI_ARCHIVER_PROJECT_OPEN_ISSUES_COUNT='null'
export GITLAB_CI_ARCHIVER_PROJECT_OWNER='null'
export GITLAB_CI_ARCHIVER_PROJECT_PATH='soc'
export GITLAB_CI_ARCHIVER_PROJECT_PATH_WITH_NAMESPACE='Kazan-team/mirrors/soc'
export GITLAB_CI_ARCHIVER_PROJECT_PERMISSIONS_GROUP_ACCESS_ACCESS_LEVEL='50'
export GITLAB_CI_ARCHIVER_PROJECT_PERMISSIONS_GROUP_ACCESS_NOTIFICATION_LEVEL='2'
export GITLAB_CI_ARCHIVER_PROJECT_PERMISSIONS_PROJECT_ACCESS='null'
export GITLAB_CI_ARCHIVER_PROJECT_PRINTING_MERGE_REQUEST_LINK_ENABLED='true'
export GITLAB_CI_ARCHIVER_PROJECT_PUBLIC_JOBS='true'
export GITLAB_CI_ARCHIVER_PROJECT_README_URL='https://salsa.debian.org/Kazan-team/mirrors/soc/-/blob/master/README.md'
export GITLAB_CI_ARCHIVER_PROJECT_REMOVE_SOURCE_BRANCH_AFTER_MERGE='true'
export GITLAB_CI_ARCHIVER_PROJECT_REPOSITORY_ACCESS_LEVEL='enabled'
export GITLAB_CI_ARCHIVER_PROJECT_REQUEST_ACCESS_ENABLED='true'
export GITLAB_CI_ARCHIVER_PROJECT_RESOLVE_OUTDATED_DIFF_DISCUSSIONS='false'
export GITLAB_CI_ARCHIVER_PROJECT_RUNNERS_TOKEN='null'
export GITLAB_CI_ARCHIVER_PROJECT_SHARED_RUNNERS_ENABLED='false'
export GITLAB_CI_ARCHIVER_PROJECT_SHARED_WITH_GROUPS_LEN='0'
export GITLAB_CI_ARCHIVER_PROJECT_SNIPPETS_ACCESS_LEVEL='enabled'
export GITLAB_CI_ARCHIVER_PROJECT_SNIPPETS_ENABLED='true'
export GITLAB_CI_ARCHIVER_PROJECT_SSH_URL_TO_REPO='git@salsa.debian.org:Kazan-team/mirrors/soc.git'
export GITLAB_CI_ARCHIVER_PROJECT_STAR_COUNT='0'
export GITLAB_CI_ARCHIVER_PROJECT_STATISTICS='null'
export GITLAB_CI_ARCHIVER_PROJECT_TAG_LIST_LEN='0'
export GITLAB_CI_ARCHIVER_PROJECT_VISIBILITY='public'
export GITLAB_CI_ARCHIVER_PROJECT_WEB_URL='https://salsa.debian.org/Kazan-team/mirrors/soc'
export GITLAB_CI_ARCHIVER_PROJECT_WIKI_ACCESS_LEVEL='enabled'
export GITLAB_CI_ARCHIVER_PROJECT_WIKI_ENABLED='true'
export GITLAB_CI_ARCHIVER_PROJECT__LINKS_EVENTS='https://salsa.debian.org/api/v4/projects/44798/events'
export GITLAB_CI_ARCHIVER_PROJECT__LINKS_ISSUES='null'
export GITLAB_CI_ARCHIVER_PROJECT__LINKS_LABELS='https://salsa.debian.org/api/v4/projects/44798/labels'
export GITLAB_CI_ARCHIVER_PROJECT__LINKS_MEMBERS='https://salsa.debian.org/api/v4/projects/44798/members'
export GITLAB_CI_ARCHIVER_PROJECT__LINKS_MERGE_REQUESTS='https://salsa.debian.org/api/v4/projects/44798/merge_requests'
export GITLAB_CI_ARCHIVER_PROJECT__LINKS_REPO_BRANCHES='https://salsa.debian.org/api/v4/projects/44798/repository/branches'
export GITLAB_CI_ARCHIVER_PROJECT__LINKS_SELF='https://salsa.debian.org/api/v4/projects/44798'
exec <<'EOF'
    addr.filename, addr.module)
  File "/builds/Kazan-team/soc/.env/lib/python3.7/site-packages/nose/importer.py", line 47, in importFromPath
    return self.importFromDir(dir_path, fqname)
  File "/builds/Kazan-team/soc/.env/lib/python3.7/site-packages/nose/importer.py", line 94, in importFromDir
    mod = load_module(part_fqname, fh, filename, desc)
  File "/usr/lib/python3.7/imp.py", line 234, in load_module
    return load_source(name, filename, file)
  File "/usr/lib/python3.7/imp.py", line 171, in load_source
    module = _load(spec)
  File "<frozen importlib._bootstrap>", line 696, in _load
  File "<frozen importlib._bootstrap>", line 677, in _load_unlocked
  File "<frozen importlib._bootstrap_external>", line 728, in exec_module
  File "<frozen importlib._bootstrap>", line 219, in _call_with_frames_removed
  File "/builds/Kazan-team/soc/src/soc/scoreboard/test_mem_fu_matrix.py", line 24, in <module>
    from ..experiment.score6600 import IssueToScoreboard, RegSim, instr_q, wait_for_busy_clear, wait_for_issue, CompUnitALUs, CompUnitBR
  File "/builds/Kazan-team/soc/src/soc/experiment/score6600.py", line 18, in <module>
    from soc.experiment.compldst import LDSTCompUnit
  File "/builds/Kazan-team/soc/src/soc/experiment/compldst.py", line 30, in <module>
    from alu_hier import CompALUOpSubset
ModuleNotFoundError: No module named 'alu_hier'

----------------------------------------------------------------------
Ran 231 tests in 30.374s

FAILED (SKIP=1, errors=3)
Running after script...
$ export CCACHE_DIR="$PWD/ccache"
$ ccache --show-stats
cache directory                     /builds/Kazan-team/soc/ccache
primary config                      /builds/Kazan-team/soc/ccache/ccache.conf
secondary config      (readonly)    /etc/ccache.conf
stats updated                       Tue Apr 21 00:29:33 2020
stats zeroed                        Tue Apr 21 00:24:22 2020
cache hit (direct)                  1804
cache hit (preprocessed)               2
cache miss                           242
cache hit rate                     88.18 %
called for link                       16
called for preprocessing               6
compile failed                         2
bad compiler arguments                 1
autoconf compile/link                  6
unsupported compiler option         1381
no input file                         15
cleanups performed                     0
files in cache                      5419
cache size                           3.0 GB
max cache size                       5.0 GB
ERROR: Job failed: exit code 1
EOF

. ./send-email.sh test